#!/bin/bash

usage()
{
  echo "Usage: lxd-launch-containers.sh [provision|destroy]"
  exit 1
}

NODES="swarm-master swarm-worker1 swarm-worker2"

containers_provision()
{
  for node in $NODES
  do
    echo "==> Bringing up $node"
    lxc launch images:debian/11 $node --vm
    echo "==> Running provisioner script"
    cat  install-docker.sh | lxc exec $node bash
    echo
  done
}

containers_destroy()
{
  for node in $NODES
  do
    echo "==> Destroying $node..."
    lxc delete --force $node
  done
}


case "$1" in
  provision)
    echo -e "\nProvisioning LXC Containers $NODES...\n"
    containers_provision
    ;;
  destroy)
    echo -e "\nLXC Containers $NODES .....\n"
    containers_destroy
    ;;
  *)
    usage
    ;;
esac


